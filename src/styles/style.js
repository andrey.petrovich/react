import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    body {
    margin: 0;
    padding: 200px;
    box-sizing: border-box;
    font-family: "Helvetica Neue", sans-serif;
    background-image: url(https://itea.ua/wp-content/uploads/2020/10/react.js.png);
    background-size: 100px;
    }
    
    h1 {
    margin: 0;
    padding: 15px;
    font-size: 16px;
    }
    
    p {
    margin: 10px 0;
    }`;

export const AppContainer = styled.div`
  padding: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 14px;`;