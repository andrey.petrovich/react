import styled from "styled-components";

export const MyModalWrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;`;

MyModalWrapper.displayName = 'Modal-wrapper';

export const MyModal = styled.div`
  background-color: ${({ className }) => {
      switch (className) {
        case 'red':
            return '#9c0000';
        case 'grey':
            return 'grey';
        default:
            return '#9c0000';
      }
  }};
  border-radius: 5px;
  min-width: 200px;
  max-width: 400px;
  color: #fff;`;

MyModal.displayName = 'Modal';

export const MyModalHeader = styled.div`
  display: flex;
  justify-content: space-between;
  border-radius: 5px 5px 0 0;

  background-color: ${({ className }) => {
      switch (className) {
        case 'red':
          return '#640b0b';
        case 'grey':
          return '#464646';
        default:
          return '#9c0000';
      }
  }};`;

export const MyModalCloseBtn = styled.div`
  padding: 15px;
  cursor: pointer;`;

export const MyModalContent = styled.div`
  padding: 25px;
  text-align: center;
  font-size: 12px;`;

export const MyModalAction = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 14px;
  padding: 5px 15px 20px;`;