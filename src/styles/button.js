import styled from "styled-components";

export const MyButton = styled.button`
  cursor: pointer;
  padding: 20px 30px;
  color: white;
  text-transform: uppercase;
  border-radius: 5px;
  transition: all 0.3s;
  border: 1px solid white;
  background-color: ${({ className }) =>{
      switch (className) {
        case 'red':
            return '#9c0000';
        case 'grey':
            return '#464646';
        default:
            return '#9c0000';
      }
  }};
  
  &:hover {
    filter: contrast(400%);
  }`;